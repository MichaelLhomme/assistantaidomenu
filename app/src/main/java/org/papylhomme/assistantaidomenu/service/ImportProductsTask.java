package org.papylhomme.assistantaidomenu.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.provider.requests.ListProductRequest;

import java.util.HashMap;



/**
 * AsyncTask to import the products from an Aidomenu CSV export
 */
class ImportProductsTask extends CSVImportTask {


    /**
     * Title of the name column
     */
    private static final String CSV_COLUMN_NAME = "Libellé Commercial";


    /**
     * Title of the family column
     */
    private static final String CSV_COLUMN_GROUP = "Groupe";


    /**
     * Title of the unit column
     */
    private static final String CSV_COLUMN_UNIT = "US";


    /**
     * Title of the brand column
     */
    private static final String CSV_COLUMN_BRAND = "Marque";



    /**
     * Constructor
     *
     * @param context The calling context instance
     * @param db An instance of the database
     */
    ImportProductsTask(Context context, SQLiteDatabase db) {
        super(context, db, ImportService.ACTION_IMPORT_PRODUCTS, DBContract.Products.TABLE_NAME);
    }



    /**
     * Process the file content to extract information and insert it in the database
     *
     * @param columns A map of available columns
     */
    @Override
    protected void processImport(HashMap<String, Integer> columns) throws Exception {
        //clear tables
        db.delete(DBContract.Products.TABLE_NAME, null, null);

        //fetch columns
        int columnName = getColumnIndex(columns, CSV_COLUMN_NAME);
        int columnGroup = getColumnIndex(columns, CSV_COLUMN_GROUP);
        int columnUnit = getColumnIndex(columns, CSV_COLUMN_UNIT);
        int columnBrand = getColumnIndex(columns, CSV_COLUMN_BRAND);

        //parse the file and add product on the fly
        String line;
        while((line = readLine()) != null) {
            //extract the information
            String[] cols = line.split(";");
            String name = cols[columnName].replaceAll("\"", "");
            String unit = cols[columnUnit].replaceAll("\"", "");
            String brand = cols[columnBrand].replaceAll("\"", "");
            int groupId = getOrCreateGroupId(cols[columnGroup].replaceAll("\"", ""));

            //finally insert the products in the database
            ContentValues values = new ContentValues();
            values.put(DBContract.Products.COLUMN_NAME, name);
            values.put(DBContract.Products.COLUMN_NORMALIZED_NAME, DBContract.Products.normalizeString(name));
            values.put(DBContract.Products.COLUMN_GROUP_ID, groupId);
            values.put(DBContract.Products.COLUMN_UNIT, unit);
            values.put(DBContract.Products.COLUMN_BRAND, brand);

            db.insert(DBContract.Products.TABLE_NAME, null, values);
        }
    }



    /**
     * Override onPostExecute to notify the content provider
     *
     * @param count The new count of products
     */
    @Override
    protected void onPostExecute(Integer count) {
        super.onPostExecute(count);
        context.getContentResolver().notifyChange(ListProductRequest.URI, null);
    }
}
