package org.papylhomme.assistantaidomenu.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.provider.requests.ListStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.StockUpdateRequest;

import java.util.HashMap;



/**
 * AsyncTask to import the stock from an Aidomenu CSV export
 */
class ImportStockTask extends CSVImportTask {


    /**
     * Title of the name column
     */
    private static final String CSV_COLUMN_NAME = "Désignation";


    /**
     * Title of the family column
     */
    private static final String CSV_COLUMN_GROUP = "Groupe";


    /**
     * Title of the unit column
     */
    private static final String CSV_COLUMN_UNIT = "US";


    /**
     * Title of the brand column
     */
    private static final String CSV_COLUMN_BRAND = "Marque";


    /**
     * Title of the amount column
     */
    private static final String CSV_COLUMN_AMOUNT = "Quantité";


    /**
     * Title of the best before date column
     */
    private static final String CSV_COLUMN_BBD = "DLC";



    /**
     * Constructor
     *
     * @param context The calling context instance
     * @param db An instance of the database
     */
    ImportStockTask(Context context, SQLiteDatabase db) {
        super(context, db, ImportService.ACTION_IMPORT_STOCK, DBContract.Stock.TABLE_NAME);
    }



    /**
     * Process the file content to extract information and insert it in the database
     *
     * @param columns A map of available columns
     */
    @Override
    protected void processImport(HashMap<String, Integer> columns) throws Exception {
        //clear tables
        db.delete(DBContract.Stock.TABLE_NAME, null, null);
        db.delete(DBContract.StockUpdate.TABLE_NAME, null, null);

        //fetch columns
        int columnName = getColumnIndex(columns, CSV_COLUMN_NAME);
        int columnGroup = getColumnIndex(columns, CSV_COLUMN_GROUP);
        int columnUnit = getColumnIndex(columns, CSV_COLUMN_UNIT);
        int columnBrand = getColumnIndex(columns, CSV_COLUMN_BRAND);
        int columnBBD = getColumnIndex(columns, CSV_COLUMN_BBD);
        int columnAmount = getColumnIndex(columns, CSV_COLUMN_AMOUNT);

        //parse the file and add stock items on the fly
        long timestamp = System.currentTimeMillis();

        String line;
        while((line = readLine()) != null) {
            //extract the information
            String[] cols = line.split(";");
            String name = cols[columnName].replaceAll("\"", "");
            int groupId = getOrCreateGroupId(cols[columnGroup].replaceAll("\"", ""));
            String unit = cols[columnUnit].replaceAll("\"", "");
            String brand = cols[columnBrand].replaceAll("\"", "");
            float amount = Float.parseFloat(cols[columnAmount].replaceAll("\"", "").replaceFirst(",", "."));

            //parse the best before date to allow sorting
            Long bbd = null;
            String bddString = cols[columnBBD].replace("\"", "").replaceAll(" 00:00:00", "");
            if(!bddString.isEmpty())
                bbd = DBContract.Stock.dateFormatter.parse(bddString).getTime();

            //finally insert the stock item in the database
            ContentValues values = new ContentValues();
            values.put(DBContract.Stock.COLUMN_TIMESTAMP, timestamp);
            values.put(DBContract.Stock.COLUMN_NAME, name);
            values.put(DBContract.Stock.COLUMN_NORMALIZED_NAME, DBContract.Stock.normalizeString(name));
            values.put(DBContract.Stock.COLUMN_GROUP_ID, groupId);
            values.put(DBContract.Stock.COLUMN_UNIT, unit);
            values.put(DBContract.Stock.COLUMN_BRAND, brand);
            values.put(DBContract.Stock.COLUMN_BBD, bbd);
            values.put(DBContract.Stock.COLUMN_CURRENT, amount);
            values.put(DBContract.Stock.COLUMN_INITIAL, amount);

            db.insert(DBContract.Stock.TABLE_NAME, null, values);
        }
    }



    /**
     * Override onPostExecute to notify the content provider
     *
     * @param count The new count of products
     */
    @Override
    protected void onPostExecute(Integer count) {
        super.onPostExecute(count);
        context.getContentResolver().notifyChange(ListStockItemRequest.URI, null);
        context.getContentResolver().notifyChange(StockUpdateRequest.URI, null);
    }
}
