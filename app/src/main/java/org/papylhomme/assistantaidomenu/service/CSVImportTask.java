package org.papylhomme.assistantaidomenu.service;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.OpenableColumns;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import static org.papylhomme.assistantaidomenu.service.ImportService.INTENT_ACTION_EVENT;
import static org.papylhomme.assistantaidomenu.service.ImportService.PARAM_EVENT_TYPE;
import static org.papylhomme.assistantaidomenu.service.ImportService.PARAM_IMPORT_COUNT;
import static org.papylhomme.assistantaidomenu.service.ImportService.PARAM_IMPORT_PROGRESS;
import static org.papylhomme.assistantaidomenu.service.ImportService.PARAM_ORIGINAL_ACTION;
import static org.papylhomme.assistantaidomenu.service.ImportService.PARAM_ERROR_MESSAGE;


/**
 * Base class for CSV import task
 */
abstract class CSVImportTask extends AsyncTask<Uri, Integer, Integer> {

    /**
     * Tag for logger
     */
    private static final String TAG = CSVImportTask.class.getName();


    /**
     * An instance of the database to use for the import
     */
    protected final SQLiteDatabase db;


    /**
     * Caller's context
     */
    protected final Context context;


    /**
     * An instance a broadcast manager
     */
    private final LocalBroadcastManager broadcastManager;


    /**
     * The import originalAction
     */
    private final String originalAction;


    /**
     * The instance of reader to the file
     */
    private BufferedReader reader;


    /**
     * The file size
     */
    private long fileSize;


    /**
     * The current position in the file
     */
    private long currentPosition;


    /**
     * The last progress update
     */
    private int lastProgress;


    /**
     * The table to import into
     */
    private String tableName;


    /**
     * A cache for product groups
     */
    private HashMap<String, Integer> groups;



    /**
     * Constructor
     *  @param context The caller's context
     * @param db An instance of the database
     * @param originalAction
     */
    CSVImportTask(Context context, SQLiteDatabase db, String originalAction, String tableName) {
        this.context = context;
        this.db = db;
        this.originalAction = originalAction;
        this.tableName = tableName;

        this.broadcastManager = LocalBroadcastManager.getInstance(context);
    }



    /**
     * Handle onPreExecute by displaying the progress bar
     */
    @Override
    protected void onPreExecute() {
        lastProgress = 0;

        Intent preExecuteEvent = new Intent();
        preExecuteEvent.putExtra(PARAM_EVENT_TYPE, ImportService.EVENT_TYPE.IMPORT_STARTED);
        broadcastEvent(preExecuteEvent);
    }



    /**
     * Handle onProgressUpdate by updating the progress bar
     *
     * @param values The current progress in percent
     */
    @Override
    protected void onProgressUpdate(Integer... values) {
        if(values[0] != lastProgress) {
            Intent progressEvent = new Intent();
            progressEvent.putExtra(PARAM_EVENT_TYPE, ImportService.EVENT_TYPE.IMPORT_PROGRESS);
            progressEvent.putExtra(PARAM_IMPORT_PROGRESS, values[0]);
            broadcastEvent(progressEvent);
        }

        lastProgress = values[0];
    }



    /**
     * Handle onPostExecute by hiding the progress bar and updating the content provider
     *
     * @param count The new count of items
     */
    @Override
    protected void onPostExecute(Integer count) {
        if(count != -1) {
            Intent postExecuteEvent = new Intent();
            postExecuteEvent.putExtra(PARAM_EVENT_TYPE, ImportService.EVENT_TYPE.IMPORT_FINISHED);
            postExecuteEvent.putExtra(PARAM_IMPORT_COUNT, count);
            broadcastEvent(postExecuteEvent);
        }
    }



    /**
     * Parse the file and process the import
     *
     * @param uris An uri to the file
     * @return Void
     */
    @Override
    protected Integer doInBackground(Uri... uris) {
        int res = -1;
        Uri uri = uris[0];

        currentPosition = 0;
        fileSize = getFileSize(uri);

        try {
            db.beginTransaction();
            groups = readGroups();

            //then parse the input stream
            InputStream is = context.getContentResolver().openInputStream(uri);
            reader = new BufferedReader(new InputStreamReader(is, "ISO-8859-15"));

            //extract the columns from the CSV header
            String[] headers = readLine().split(";");
            HashMap<String, Integer> indexesByName = new HashMap<>();
            for(int i = 0; i < headers.length; i++)
                indexesByName.put(headers[i], i);

            //process the import
            processImport(indexesByName);

            //end the import
            reader.close();
            db.setTransactionSuccessful();

            //fetch the number of items
            Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + tableName, null);
            cursor.moveToFirst();
            res = cursor.getInt(0);
            cursor.close();

        } catch(ColumnNotFoundException e) {
            Log.e(TAG, "Error importing file", e);
            res = -1;

            Intent errorEvent = new Intent();
            errorEvent.putExtra(PARAM_EVENT_TYPE, ImportService.EVENT_TYPE.IMPORT_ERROR);
            errorEvent.putExtra(PARAM_ERROR_MESSAGE, context.getString(R.string.sb_import_error_column_not_found, e.column));
            broadcastEvent(errorEvent);

        } catch(Exception e) {
            Log.e(TAG, "Error importing file", e);
            res = -1;

            Intent errorEvent = new Intent();
            errorEvent.putExtra(PARAM_EVENT_TYPE, ImportService.EVENT_TYPE.IMPORT_ERROR);
            errorEvent.putExtra(PARAM_ERROR_MESSAGE, e.toString());
            broadcastEvent(errorEvent);

        } finally {
             db.endTransaction();
        }

        return res;
    }



    /**
     * Process the file content to extract information and insert it in the database
     *
     * @param columns A map of available columns
     */
    protected abstract void processImport(HashMap<String, Integer> columns) throws Exception;



    /**
     * Read a line from the file
     *
     * @return A line, or null if the reader is not opened
     */
    protected String readLine() throws IOException {
        //sanity check
        if(reader == null)
            return null;

        //update the position and the progress
        String line = reader.readLine();
        if(line != null) {
            currentPosition = currentPosition + line.length() + 2;
            publishProgress((int) Math.round(currentPosition * 100.0 / fileSize));
        }

        return line;
    }



    /**
     * Get the index of the wanted column or throw an exception
     *
     * @param columns A list of available columns
     * @param name The wanted column
     * @return The index of the column
     * @throws Exception
     */
    protected int getColumnIndex(HashMap<String, Integer> columns, String name) throws ColumnNotFoundException {
        if(!columns.containsKey(name))
            throw new ColumnNotFoundException(name);

        return columns.get(name);
    }



    /**
     * Get the size of a file
     *
     * @param uri An uri to the file
     * @return The size of the file
     */
    private long getFileSize(Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        long size = cursor.getLong(cursor.getColumnIndex(OpenableColumns.SIZE));
        cursor.close();
        return size;
    }



    /**
     * Helper to broadcast an event
     *
     * @param event An intent containing the event
     */
    private void broadcastEvent(Intent event) {
        event.setAction(INTENT_ACTION_EVENT);
        event.putExtra(PARAM_ORIGINAL_ACTION, originalAction);
        broadcastManager.sendBroadcast(event);
    }



    /**
     * Initialize a map of product groups
     *
     * @return A map of product groups
     */
    private HashMap<String, Integer> readGroups() {
        HashMap<String, Integer> groups = new HashMap<>();

        Cursor cursor = db.query(DBContract.ProductGroup.TABLE_NAME, null, null, null, null, null, null);
        int columnId = cursor.getColumnIndex(DBContract.ProductGroup._ID);
        int columnName = cursor.getColumnIndex(DBContract.ProductGroup.COLUMN_NAME);

        while(cursor.moveToNext()) {
            groups.put(cursor.getString(columnName), cursor.getInt(columnId));
        }

        return groups;
    }



    /**
     * Get the id of an existing group or confirmMerge a new one if needed
     *
     * @param group
     * @return A group id
     */
    protected int getOrCreateGroupId(String group) {
        if(groups.containsKey(group))
            return groups.get(group);

        ContentValues values = new ContentValues();
        values.put(DBContract.ProductGroup.COLUMN_NAME, group);
        int id = (int) db.insert(DBContract.ProductGroup.TABLE_NAME, null, values);

        groups.put(group, id);

        return id;
    }



    /**
     * An column not found exception
     */
    class ColumnNotFoundException extends Exception {

        /**
         * The column's name
         */
        private final String column;


        /**
         * Constructor
         *
         * @param column The column's name
         */
        ColumnNotFoundException(String column) {
            super("Column not found: '" + column + "'");
            this.column = column;

        }


        /**
         * Column accessor
         * @return The column's name
         */
        public String getColumn() {
            return column;
        }
    }
}
