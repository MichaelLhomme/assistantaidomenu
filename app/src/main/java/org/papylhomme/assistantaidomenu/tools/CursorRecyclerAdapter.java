package org.papylhomme.assistantaidomenu.tools;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



/**
 * An adapter implementation using cursor as a backend
 */
public abstract class CursorRecyclerAdapter<Item, ViewHolder extends CursorViewHolder<Item>> extends RecyclerView.Adapter<ViewHolder> {


    /**
     * The current cursor instance
     */
    private Cursor cursor;


    /**
     * The layout resource for items
     */
    private int layout;



    /**
     * Constructor
     *
     * @param layout A layout resource for items
     */
    public CursorRecyclerAdapter(int layout) {
        this.layout = layout;
    }



    /**
     * Instantiate a view holder with the given item view
     *
     * @param view A view created with the layout resource
     * @return A view holder instance
     */
    protected abstract ViewHolder createViewHolder(View view);



    /**
     * Create an item for the given cursor position
     *
     * @param cursor An instance of cursor
     * @return An item instance
     */
    protected abstract Item createItem(Cursor cursor);



    /**
     * Create a view holder
     *
     * @param parent The view parent
     * @param viewType Type of the view. Unused
     * @return A view holder instance
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return createViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout, parent, false));
    }



    /**
     * Bind a view holder to the item at the given position
     *
     * @param holder A view holder instance
     * @param position The position of the item to bind
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(getItem(position));
    }



    /**
     * Get the number of items in the adapter
     *
     * @return The number of items
     */
    @Override
    public int getItemCount() {
        if(cursor == null)
            return 0;
        else
            return cursor.getCount();
    }



    /**
     * Set the backend cursor for the adapter
     *
     * Do not close the old cursor as the loader backend will take care of it
     *
     * @param cursor A cursor instance, or null to reset the adapter
     */
    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
    }



    /**
     * Get the item at the given position
     *
     * @param position The position of the wanted item
     * @return An item instance
     */
    public Item getItem(int position) {
        cursor.moveToPosition(position);
        return createItem(cursor);
    }
}
