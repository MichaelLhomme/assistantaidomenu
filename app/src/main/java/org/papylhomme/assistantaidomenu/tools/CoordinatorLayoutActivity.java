package org.papylhomme.assistantaidomenu.tools;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.papylhomme.assistantaidomenu.R;



/**
 * Helper class for activities using CoordinatorLayout
 */
public class CoordinatorLayoutActivity extends AppCompatActivity {


    /**
     * The floating action button instance
     */
    private FloatingActionButton fab;


    /**
     * A FABController instance
     */
    private FABController fabController;



    /**
     * Override to setup the FAB
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.base_layout);

        this.fab = (FloatingActionButton) findViewById(R.id.fab);

        this.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fabController != null)
                    fabController.onFABClickListener(view);
            }
        });


        //auto register activity if it implements FABController
        if(this instanceof FABController)
            installFABController((FABController) this);
        else
            uninstallFABController();
    }



    /**
     *
     * @param controller
     */
    protected void installFABController(FABController controller) {
        this.fabController = controller;

        if(this.fabController != null) {
            fab.setImageResource(this.fabController.getFABImageResource());
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setImageResource(0);
            fab.setVisibility(View.GONE);
        }
    }



    /**
     *
     */
    protected void uninstallFABController() {
        installFABController(null);
    }



    /**
     * Describe the interface for FAB controllers
     */
    public interface FABController {

        /**
         * Retrieve an image resource for the FAB
         *
         * @return An image resource
         */
        int getFABImageResource();


        /**
         * Handle FAB clicked
         *
         * @param view The clicked view
         */
        void onFABClickListener(View view);

    }
}
