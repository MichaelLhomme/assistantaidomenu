package org.papylhomme.assistantaidomenu.tools;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.ui.DialogGroupFilter;

import java.util.ArrayList;


/**
 * Base class for searchable list fragment
 */
public abstract class SearchableRecyclerListFragment<Item, ViewHolder extends CursorViewHolder<Item>>
        extends RecyclerListFragment<Item, ViewHolder>
        implements DialogGroupFilter.OnGroupFilterChangeListener {

    /**
     * Parameter name to store the search
     */
    private static final String PARAM_SEARCH_QUERY = "SEARCH_QUERY";


    /**
     * The instance of search view
     */
    private SearchView searchView;


    /**
     * An instance of searchable list filters listener
     */
    protected SearchableListFiltersListener searchableListFiltersListener;



    /**
     * Test if there is an active search
     *
     * @return True if there is an active search, false otherwise
     */
    public boolean hasSearchQuery() {
        return getArguments().containsKey(PARAM_SEARCH_QUERY);
    }



    /**
     * Retrieve the search query
     *
     * @return The search query
     */
    public String getSearchQuery() {
        return getArguments().getString(PARAM_SEARCH_QUERY);
    }



    /**
     * Test if there is an active group filter
     *
     * @return True if there is an active group filter, false otherwise
     */
    public boolean hasGroupFilter() {
        return getArguments().containsKey(DialogGroupFilter.PARAM_GROUPS_FILTER);
    }



    /**
     * Retrieve the group filter
     *
     * @return The group filter
     */
    public ArrayList<Integer> getGroupFilter() {
        return getArguments().getIntegerArrayList(DialogGroupFilter.PARAM_GROUPS_FILTER);
    }



    /**
     * Handle onCreate to enable the fragment menu
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    /**
     * Handle onCreateOptionsMenu to inflate and initialize the search menu
     *
     * @param menu An instance of menu
     * @param inflater An instance of menu inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        searchView = (SearchView) menu.findItem(R.id.search_view).getActionView();
        if(searchView != null)
            setupSearchView();
    }



    /**
     * Override onOptionsItemSelected to handle group filtering
     *
     * @param item The select menu item
     * @return True if the event was handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_groups_filter) {
            DialogGroupFilter groupFilter = new DialogGroupFilter();

            Bundle args = getGroupFilterArguments();
            args.putIntegerArrayList(DialogGroupFilter.PARAM_GROUPS_FILTER, getGroupFilter());

            groupFilter.setArguments(args);
            groupFilter.setTargetFragment(this, 0);
            groupFilter.show(getFragmentManager(), "group_filter");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * Initialize arguments for group filter dialog
     *
     * @return A bundle or null for no arguments
     */
    protected Bundle getGroupFilterArguments() {
        return new Bundle();
    }



    /**
     * Override to setup the filters listener
     *
     * @param context The parent activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof SearchableListFiltersListener)
            this.searchableListFiltersListener = (SearchableListFiltersListener) context;
        else
            this.searchableListFiltersListener = null;
    }



    /**
     * Override to clear the filters listener
     */
    @Override
    public void onDetach() {
        super.onDetach();
        this.searchableListFiltersListener = null;
    }



    /**
     * Handle onPause to clear the search view focus in order
     * to close the keyboard
     */
    @Override
    public void onPause() {
        searchView.clearFocus();
        super.onPause();
    }



    /**
     * Handle onGroupFilterChange
     *
     * @param groupsFilter The new group filter
     */
    @Override
    public void onGroupFilterChange(ArrayList<Integer> groupsFilter) {
        //store groups filter
        if(groupsFilter == null)
            getArguments().remove(DialogGroupFilter.PARAM_GROUPS_FILTER);
        else
            getArguments().putIntegerArrayList(DialogGroupFilter.PARAM_GROUPS_FILTER, groupsFilter);

        //notify activity
        if(searchableListFiltersListener != null)
            searchableListFiltersListener.updateGroupsFilter(groupsFilter);
    }



    /**
     * Setup the search view
     */
    private void setupSearchView() {
        ((ImageView) searchView.findViewById (android.support.v7.appcompat.R.id.search_button)).setImageResource(R.drawable.icon_search);

        //set the content of the search view from fragment parameters
        String searchQuery = getArguments().getString(PARAM_SEARCH_QUERY);

        if(searchQuery == null)
            searchView.setIconified(true);
        else {
            searchView.setIconified(false);
            searchView.setQuery(searchQuery, false);
            searchView.clearFocus();
        }

        //register change listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(!isRemoving()) {
                    if (s.isEmpty())
                        getArguments().remove(PARAM_SEARCH_QUERY);
                    else
                        getArguments().putString(PARAM_SEARCH_QUERY, s);

                    if(searchableListFiltersListener != null)
                        searchableListFiltersListener.updateSearchQuery(s);
                }

                return true;
            }
        });
    }



    /**
     * Describe the interface for the activity list filters
     */
    public interface SearchableListFiltersListener {


        /**
         * Update the search query for the list
         *
         * @param query A search query
         */
        void updateSearchQuery(String query);


        /**
         * Update the groups filter for the list
         *
         * @param groups An array of groups to select for the list
         */
        void updateGroupsFilter(ArrayList<Integer> groups);

    }

}
