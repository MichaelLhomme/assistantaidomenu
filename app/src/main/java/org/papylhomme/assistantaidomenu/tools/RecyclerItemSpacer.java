package org.papylhomme.assistantaidomenu.tools;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

/**
 * Item decorator for spacing the cards
 */
public class RecyclerItemSpacer extends RecyclerView.ItemDecoration {

    /**
     * Spacing for the items, in pixels
     */
    private int spacing;


    /**
     * Constructor with spacing as parameter
     *
     * @param spacing The spacing to use for items, in dp
     */
    public RecyclerItemSpacer(Context ctx, int spacing) {
        this.spacing = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, spacing,  ctx.getResources().getDisplayMetrics()));
    }



    /**
     * Override getItemOffsets to add spacing to items
     *
     * @param outRect Offsets for the current item
     * @param item The recycler item
     * @param parent The recycler view
     * @param state Unused
     */
    @Override
    public void getItemOffsets(Rect outRect, View item, RecyclerView parent, RecyclerView.State state) {
        outRect.left = 0;
        outRect.right = 0;
        outRect.top = 0;
        outRect.bottom = spacing;

        // Add top margin only for the first item to avoid double space between items
        if(parent.getChildAdapterPosition(item) == 0)
            outRect.top = spacing;
    }
}
