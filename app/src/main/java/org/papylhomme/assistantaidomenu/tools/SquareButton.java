package org.papylhomme.assistantaidomenu.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;


/**
 *
 */
public class SquareButton extends TextView {


    /**
     *
     * @param context
     */
    public SquareButton(Context context) {
        super(context);
        init();
    }



    /**
     *
     * @param context
     * @param attrs
     */
    public SquareButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }



    /**
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public SquareButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    /**
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     */
    public SquareButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }



    private void init() {
        setClickable(true);
        setGravity(Gravity.CENTER);
        setTag("calculator_buttons");
    }


    /**
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        int size = width > height ? height : width;
        if(getParent() instanceof View) {
            View parent = (View) getParent();

            if(parent.getWidth() < parent.getHeight())
                size = width > height ? width : height;
            else
                size = width > height ? height : width;
        }


        setMeasuredDimension(size, size);
    }
}
