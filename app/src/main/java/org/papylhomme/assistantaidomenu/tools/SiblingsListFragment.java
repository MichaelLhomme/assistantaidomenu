package org.papylhomme.assistantaidomenu.tools;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.papylhomme.assistantaidomenu.R;



/**
 * A Fragment displaying a list of siblings using a ViewPager
 */
public abstract class SiblingsListFragment extends Fragment implements CursorListFragment {

    /**
     * Parameter for product id
     */
    protected static String PARAM_POSITION = "PRODUCT_POSITION";


    /**
     * Parameter for product id
     */
    protected static String PARAM_SEARCH_QUERY = "SEARCH_QUERY";


    /**
     * Parameter for product id
     */
    protected static String PARAM_GROUP_FILTER = "GROUP_FILTER";


    /**
     * The adapter for the pager
     */
    protected SiblingsAdapter adapter;


    /**
     * Current position in the pager
     */
    protected int position = -1;


    /**
     * The pager instance
     */
    protected ViewPager pager;


    /**
     * Thex page count label instance
     */
    protected TextView labelPageCount;



    /**
     * Override to extract the current position to display
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.position = getArguments().getInt(PARAM_POSITION, 0);
    }



    /**
     * Initialize the container view
     *
     * @param inflater A layout inflater instance
     * @param container A view container instance
     * @param savedInstanceState A saved instance
     * @return The root view of the fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.siblings_list_fragment, container, false);

        labelPageCount = (TextView) rootView.findViewById(R.id.label_page_count);

        pager = (ViewPager) rootView.findViewById(R.id.pager_siblings);
        adapter = new SiblingsAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);


        //handle on page change to update the current position
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                SiblingsListFragment.this.position = position;
                updatePageCountLabel();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return rootView;
    }



    /**
     * Update the list of items with the given cursor
     *
     * @param cursor A cursor wrapping the items
     */
    @Override
    public void updateItems(Cursor cursor) {
        adapter.setCursor(cursor);

        if(position >= 0 && position < pager.getAdapter().getCount())
            pager.setCurrentItem(position, false);

        else if(position >= 0 && pager.getAdapter().getCount() > 0)
            pager.setCurrentItem(pager.getAdapter().getCount() - 1, false);

        updatePageCountLabel();
    }



    /**
     * Update the page count indicator
     */
    @SuppressLint("DefaultLocale")
    private void updatePageCountLabel() {
        if(position == -1 || adapter.getCount() == 0)
            labelPageCount.setText("");
        else
            labelPageCount.setText(String.format("%d / %d", position + 1, adapter.getCount()));
    }



    /**
     * Callback to create a child fragment
     *
     * @param id Id of the item to display in the fragment
     * @return A fragment instance
     */
    protected abstract Fragment createSiblingFragment(int id);



    /**
     * Get the id of the currently displayed item
     *
     * @return An item id
     */
    public int getCurrentItemId() {
        return adapter.getItemId(pager.getCurrentItem());
    }



    /**
     * Adapter for the view pager
     */
    public class SiblingsAdapter extends FragmentStatePagerAdapter {

        /**
         * A cursor instance
         */
        private Cursor cursor;



        /**
         * Constructor
         *
         * @param fm A FragmentManager to managed the children
         */
        SiblingsAdapter(FragmentManager fm) {
            super(fm);
        }



        /**
         * Return the number of views available.
         */
        @Override
        public int getCount() {
            return cursor == null ? 0 : cursor.getCount();
        }



        /**
         * Return the Fragment associated with a specified position.
         *
         * @param position The position in the list
         */
        @Override
        public Fragment getItem(int position) {
            if(cursor == null || cursor.getCount() == 0)
                return null;

            if(cursor.moveToPosition(position))
                return createSiblingFragment(cursor.getInt(cursor.getColumnIndex(BaseColumns._ID)));
            else if(cursor.moveToLast())
                return createSiblingFragment(cursor.getInt(cursor.getColumnIndex(BaseColumns._ID)));
            else
                return null;
        }



        /**
         * Override getItemPosition to force the recreation of fragment after a dataset change
         *
         * @param object Unused
         * @return A position for the given object, or POSITION_NONE
         */
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE; //TODO currently force recreation on dataset change, maybe look for the id in cursor or too expensive ?
        }



        /**
         * Update the cursor
         *
         * @param cursor A cursor instance
         */
        public void setCursor(Cursor cursor) {
            this.cursor = cursor;
            notifyDataSetChanged();
        }



        /**
         * Get the id of the item at the given position
         *
         * @param position The position
         */
        int getItemId(int position) {
            if(cursor.moveToPosition(position)) {
                return cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
            } else
                return -1;
        }
    }
}
