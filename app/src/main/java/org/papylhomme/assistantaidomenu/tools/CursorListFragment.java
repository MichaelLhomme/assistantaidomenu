package org.papylhomme.assistantaidomenu.tools;

import android.database.Cursor;



/**
 * Describe the interface for ListFragment backed by a cursor
 */
public interface CursorListFragment {

    /**
     * Update the list of items with the given cursor
     *
     * @param cursor A cursor wrapping the items
     */
    void updateItems(Cursor cursor);

}
