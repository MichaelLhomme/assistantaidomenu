package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBContract.Products;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * A request to create a new stock item from a product
 */
public class NewStockItemRequest extends ContentRequestHandler {

    /**
     * The Uri for stock items
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + Stock.TABLE_NAME);


    /**
     * Parameter name for the product id
     */
    private static final String PARAM_PRODUCT_ID = "PRODUCT_ID";



    /**
     * Constructor
     *
     * @param context  A context instance
     * @param dbHelper A DBHelper instance
     */
    public NewStockItemRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Stock.TABLE_NAME + "/import", 0);
    }



    /**
     * Get a mime type for the given request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.item/" + AUTHORITY + "/" + Stock.TABLE_NAME;
    }



    /**
     * Handle insert request
     *
     * @param uri A request uri
     * @param contentValues Values to insert
     * @return The inserted uri
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            String productId = Integer.toString(contentValues.getAsInteger(PARAM_PRODUCT_ID));
            db.beginTransactionNonExclusive();

            Cursor cursor = db.query(Products.TABLE_NAME, null, Products._ID + " = ?", new String[]{productId}, null, null, null);
            cursor.moveToFirst();
            String name = cursor.getString(cursor.getColumnIndex(Products.COLUMN_NAME));
            long groupId = cursor.getLong(cursor.getColumnIndex(Products.COLUMN_GROUP_ID));
            String unit = cursor.getString(cursor.getColumnIndex(Products.COLUMN_UNIT));
            cursor.close();

            ContentValues values = new ContentValues();
            values.put(Stock.COLUMN_NAME, name);
            values.put(Stock.COLUMN_NORMALIZED_NAME, Stock.normalizeString(name));
            values.put(Stock.COLUMN_GROUP_ID, groupId);
            values.put(Stock.COLUMN_UNIT, unit);

            values.put(Stock.COLUMN_CURRENT, 0);
            values.put(Stock.COLUMN_INITIAL, 0);
            values.put(Stock.COLUMN_TIMESTAMP, System.currentTimeMillis());

            long stockItemId = (int) db.insert(Stock.TABLE_NAME, null, values);

            db.setTransactionSuccessful();

            context.getContentResolver().notifyChange(URI, null);
            return Uri.withAppendedPath(URI, Long.toString(stockItemId));

        } catch(Exception ex) {
            Log.e(NewStockItemRequest.class.getName(), "Error inserting new stock item from product", ex);
            return null;

        } finally {
            db.endTransaction();
        }
    }



    /**
     * Helper to insert a new stock item from a product
     *
     * @param context A context instance
     * @param productId A product id
     */
    public static void insertNewStockItem(Context context, int productId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NewStockItemRequest.PARAM_PRODUCT_ID, productId);
        context.getContentResolver().insert(Uri.withAppendedPath(NewStockItemRequest.URI, "/import"), contentValues);
    }
}
