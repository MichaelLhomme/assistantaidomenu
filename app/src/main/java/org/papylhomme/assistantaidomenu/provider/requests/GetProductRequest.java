package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.Products;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request a product
 */
public class GetProductRequest extends BaseProductsRequest {


    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public GetProductRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Products.TABLE_NAME + "/#", 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.item/" + AUTHORITY + "/" + Products.TABLE_NAME;
    }



    /**
     * Create a loader to fetch a product
     *
     * @param context A context instance
     * @param productId The id of the product to load
     * @return A loader instance
     */
    public static CursorLoader get(Context context, int productId) {
        String stringId = Integer.toString(productId);
        Query.Selection selection = new Query.Selection().match(Query.fqcn(Products.TABLE_NAME, Products._ID), stringId);

        return createLoader(Uri.withAppendedPath(URI, stringId), context, getDefaultProjection(), null, selection);
    }
}
