package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;
import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBContract.StockUpdate;
import org.papylhomme.assistantaidomenu.db.DBHelper;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request the merge of stock updates
 */
public class MergeStockUpdateRequest extends ContentRequestHandler {


    /**
     * The Uri for stock items update
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + Stock.TABLE_NAME + "/merge");



    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public MergeStockUpdateRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Stock.TABLE_NAME + "/merge/#", 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.item/" + AUTHORITY + "/" + Stock.TABLE_NAME;
    }



    /**
     * Perform a merge of stock updates
     *
     * @param uri A request uri
     * @param contentValues Values to update
     * @param selection Selection for the request
     * @param args Arguments for selection
     * @return The number of affected rows
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] args) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String stockItemId = uri.getLastPathSegment();
        String itemSelection = Stock._ID + " = ?";
        String[] itemSelectionArgs = new String[]{stockItemId};

        try {
            db.beginTransaction();

            //fetch current quantity
            Cursor c = db.query(Stock.TABLE_NAME, null, itemSelection, itemSelectionArgs, null, null, null);
            c.moveToFirst();
            double current = c.getDouble(c.getColumnIndex(Stock.COLUMN_CURRENT));
            c.close();

            //set initial quantity to current and update timestamp
            ContentValues stockValues = new ContentValues();
            stockValues.put(Stock.COLUMN_INITIAL, current);
            stockValues.put(Stock.COLUMN_TIMESTAMP, System.currentTimeMillis());
            db.update(Stock.TABLE_NAME, stockValues, itemSelection, itemSelectionArgs);

            //remove related stock updates
            db.delete(StockUpdate.TABLE_NAME, StockUpdate.COLUMN_STOCK_ITEM_ID + " = ?", itemSelectionArgs);

            db.setTransactionSuccessful();

            //notify
            notifyChange(Uri.withAppendedPath(GetStockItemRequest.URI, stockItemId));
            notifyChange(Uri.withAppendedPath(StockUpdateRequest.URI, stockItemId));
            notifyChange(Uri.withAppendedPath(ListUpdatedStockItemRequest.URI, stockItemId));

        } finally {
            db.endTransaction();
        }

        return 1;
    }



    /**
     * Request a merge of stock request
     *
     * @param context A context instance
     * @param stockItemId A stock item id
     */
    public static void mergeStock(Context context, int stockItemId) {
        String stringId = Integer.toString(stockItemId);

        context.getContentResolver().update(
                Uri.withAppendedPath(URI, stringId),
                new ContentValues(),
                null,
                null);
    }
}
