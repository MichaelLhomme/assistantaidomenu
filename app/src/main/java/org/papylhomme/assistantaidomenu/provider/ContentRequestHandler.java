package org.papylhomme.assistantaidomenu.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;


/**
 * Base class for content request handler
 */
public abstract class ContentRequestHandler {


    /**
     * An context instance
     */
    protected final Context context;


    /**
     * An uri matcher instance
     */
    protected final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);


    /**
     * An instance of the database helper
     */
    protected DBHelper dbHelper;



    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public ContentRequestHandler(Context context, DBHelper dbHelper) {
        this.context = context;
        this.dbHelper = dbHelper;
    }



    /**
     * Add a new uri to match
     *
     * @param path Path for the uri
     * @param code Result code when matching the uri
     */
    protected void addUri(String path, int code) {
        uriMatcher.addURI(DataProvider.AUTHORITY, path, code);
    }



    /**
     * Test whether the handler match the uri or not
     *
     * @param uri An uri
     * @return True if the handler match the uri, false otherwise
     */
    public boolean match(Uri uri) {
        return uriMatcher.match(uri) != UriMatcher.NO_MATCH;
    }



    /**
     * Handle a query
     *
     * @param uri A request uri
     * @param projection Projection for the query
     * @param selection Selection for the query
     * @param args Arguments for selection
     * @param sortOrder A sort order
     * @return A cursor wrapping the results
     */
    @SuppressWarnings("unused")
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        return Query.emptyCursor();
    }



    /**
     * Get a mime type for the given request
     *
     * @param uri A request uri
     * @return A mime type
     */
    abstract public String getType(Uri uri);



    /**
     * Insert new values
     *
     * @param uri A request uri
     * @param contentValues Values to insert
     * @return The number of affected rows
     */
    @SuppressWarnings("unused")
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }



    /**
     * Delete values
     *
     * @param uri A request uri
     * @param selection Selection for the request
     * @param args Arguments for selection
     * @return The number of deleted rows
     */
    @SuppressWarnings("unused")
    public int delete(Uri uri, String selection, String[] args) {
        return 0;
    }



    /**
     * Update values
     *
     * @param uri A request uri
     * @param contentValues Values to update
     * @param selection Selection for the request
     * @param args Arguments for selection
     * @return The number of updated rows
     */
    @SuppressWarnings("unused")
    public int update(Uri uri, ContentValues contentValues, String selection, String[] args) {
        return 0;
    }



    /**
     * Notify a change for the given uri
     *
     * @param uri The base uri to update
     */
    protected void notifyChange(Uri uri) {
        context.getContentResolver().notifyChange(uri, null);
    }



    /**
     * Create a loader
     *
     * @param uri A request Uri
     * @param context A context instance
     * @param projection Projection for the request
     * @param sort Sort directive for the request
     * @param selection A selection for the request
     * @return A loader instance
     */
    protected static CursorLoader createLoader(Uri uri, Context context, String[] projection, String sort, Query.Selection selection) {
        String selectionString = selection == null ? null : selection.getString();
        String[] args = selection == null ? null : selection.getArguments();

        return new CursorLoader(context,
                uri,
                projection,
                selectionString,
                args,
                sort);
    }


}
