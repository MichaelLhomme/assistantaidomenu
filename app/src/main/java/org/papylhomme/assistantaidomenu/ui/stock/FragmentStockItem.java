package org.papylhomme.assistantaidomenu.ui.stock;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.db.DBStockItem;
import org.papylhomme.assistantaidomenu.provider.requests.GetStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.StockUpdateRequest;

import java.text.DateFormat;
import java.util.Date;



/**
 * A fragment to display a stock item
 */
public class FragmentStockItem extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * Parameter for stock item id
     */
    private static String PARAM_STOCK_ITEM_ID = "STOCK_ITEM_ID";


    /**
     * Instance of the stock item to display
     */
    private DBStockItem stockItem;


    /**
     * Simple cursor adapter for the stock updates
     */
    private SimpleCursorAdapter adapter;


    /**
     * A date formatter instance
     */
    final DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);



    /**
     * Override onCreate to enable fragment's menu
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    /**
     * Handle onResume by restarting the loaders
     */
    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(0, null, this);
        getLoaderManager().restartLoader(1, null, this);
    }



    /**
     * Create the layout view
     *
     * @param inflater An inflater instance
     * @param container A view container
     * @param savedInstanceState A saved instance
     * @return The root view of the fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stock_item, container, false);

        //setup stock update dialog
        View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(stockItem != null) {
                        DialogStockUpdate dialog = new DialogStockUpdate();
                        dialog.setArguments(DialogStockUpdate.parameters(stockItem));
                        dialog.show(getFragmentManager(), "stock_update");
                    }
                }
        };

        rootView.findViewById(R.id.label_stock_item_current).setOnClickListener(listener);
        rootView.findViewById(R.id.label_stock_item_unit).setOnClickListener(listener);


        //setup stock update list
        adapter = new StockUpdateAdapter();
        ListView list = (ListView) rootView.findViewById(R.id.list_stock_update);
        list.setAdapter(adapter);

        updateView(rootView);

        return rootView;
    }



    /**
     * Override onCreateOptionsMenu to install stock item menu
     *
     * @param menu A menu intance
     * @param inflater A menu inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_stock_item, menu);
    }



    /**
     * Override onOptionsItemSelected to handle stock item actions
     *
     * @param item The selected menu item
     * @return True if the event was handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_merge_stock_update) {
            DialogConfirmMergeStockUpdate.confirmMerge(getActivity(), stockItem.getId());
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }



    /**
     * Update the content of the view with the current stock item instance
     *
     * @param rootView The root view to update
     */
    @SuppressLint("DefaultLocale")
    private void updateView(View rootView) {
        //sanity check
        if(rootView == null)
            return;

        TextView name = (TextView) rootView.findViewById(R.id.label_stock_item_name);
        TextView group = (TextView) rootView.findViewById(R.id.label_stock_item_group);
        TextView brand = (TextView) rootView.findViewById(R.id.label_stock_item_brand);
        TextView unit = (TextView) rootView.findViewById(R.id.label_stock_item_unit);
        TextView bbd = (TextView) rootView.findViewById(R.id.label_stock_item_bbd);
        TextView current = (TextView) rootView.findViewById(R.id.label_stock_item_current);

        TextView initialQuantity = (TextView) rootView.findViewById(R.id.label_stock_update_amount);
        TextView initialTimestamp = (TextView) rootView.findViewById(R.id.label_stock_update_timestamp);
        TextView initialType = (TextView) rootView.findViewById(R.id.label_stock_update_type);

        CardViewStockItem cardView = (CardViewStockItem) rootView.findViewById(R.id.cardview_stock_item);
        cardView.setStockItem(stockItem);

        String unknown = getString(android.R.string.unknownName);

        if(stockItem == null) {
            name.setText(unknown);
            group.setText(unknown);
            brand.setText(unknown);
            unit.setText(unknown);
            bbd.setText(unknown);
            current.setText(unknown);

            initialQuantity.setText(unknown);
            initialTimestamp.setText(unknown);
            initialType.setText(unknown);

        } else {
            name.setText(stockItem.getName());
            group.setText(stockItem.getGroup());
            brand.setText(stockItem.getBrand());
            unit.setText(stockItem.getUnit());
            bbd.setText(stockItem.getFormattedBbd());

            current.setText(String.format("%.2f", stockItem.getCurrentQuantity()));
            initialQuantity.setText(String.format("%.2f", stockItem.getInitialQuantity()));

            initialTimestamp.setText(dateFormatter.format(new Date(stockItem.getInitialTimestamp())));
            initialType.setText(getString(R.string.label_inital));
        }
    }



    /**
     * Create a loader to fetch information
     *
     * @param i The loader id
     * @param bundle Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        int stockItemId = -1;
        Bundle params = getArguments();
        if(params.containsKey(PARAM_STOCK_ITEM_ID))
            stockItemId = getArguments().getInt(PARAM_STOCK_ITEM_ID);

        switch(i) {
            case 0: return GetStockItemRequest.get(getActivity(), stockItemId);
            case 1: return StockUpdateRequest.list(getActivity(), stockItemId);
            default: return null;
        }
    }



    /**
     * Handle loaders results
     *
     * @param loader The loader instance
     * @param cursor A cursor instance
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch(loader.getId()) {
            case 0:
                stockItem = cursor.moveToFirst() ? new DBStockItem(cursor) : null;
                updateView(getView());
                break;

            case 1:
                adapter.swapCursor(cursor);
                break;
        }
    }



    /**
     * Handle loaderRest by resetting the view
     *
     * @param loader The loader instance
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch(loader.getId()) {
            case 0:
                stockItem = null;
                updateView(getView());
                break;

            case 1:
                adapter.swapCursor(null);
                break;
        }
    }



    /**
     * Create a bundle of parameters for a FragmentProduct instance
     *
     * @param stockItem The stockItem to pass as parameters
     * @return A bundle instance
     */
    public static Bundle parameters(DBStockItem stockItem) {
        Bundle args = new Bundle();
        args.putInt(PARAM_STOCK_ITEM_ID, stockItem.getId());
        return args;
    }



    /**
     * Create a bundle of parameters for a FragmentProduct instance
     *
     * @param id The stock item id
     * @return A bundle instance
     */
    public static Bundle parameters(int id) {
        Bundle args = new Bundle();
        args.putInt(PARAM_STOCK_ITEM_ID, id);
        return args;
    }



    /**
     * Simple adapter for stock updates
     */
    class StockUpdateAdapter extends SimpleCursorAdapter {

        /**
         * Source columns
         */
        final String[] from = new String[] {
                DBContract.StockUpdate.COLUMN_TIMESTAMP,
                DBContract.StockUpdate.COLUMN_AMOUNT,
                DBContract.StockUpdate.COLUMN_TYPE
        };


        /**
         * Destination resources
         */
        final int[] to = new int[] {
                R.id.label_stock_update_timestamp,
                R.id.label_stock_update_amount,
                R.id.label_stock_update_type,
        };



        /**
         * Constructor
         */
        StockUpdateAdapter() {
            super(getActivity(), R.layout.list_item_stock_update, null, null, null, 0);
            changeCursorAndColumns(null, from, to);
        }



        /**
         * Set the content of the row
         *
         * @param v A text view
         * @param text The text to set
         */
        @Override
        public void setViewText(TextView v, String text) {
            if(v.getId() == R.id.label_stock_update_timestamp) {
                v.setText(dateFormatter.format(new Date(Long.parseLong(text))));
            } else
                super.setViewText(v, text);
        }
    }
}
