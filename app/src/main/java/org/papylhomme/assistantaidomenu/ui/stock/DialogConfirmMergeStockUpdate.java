package org.papylhomme.assistantaidomenu.ui.stock;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.provider.requests.MergeStockUpdateRequest;

/**
 * Simple helper to confirmMerge a confirmation dialog before merging stock updates
 */
class DialogConfirmMergeStockUpdate {



    /**
     * Display a dialog and call merge stock if the user confirms
     *
     * @param context A context instance
     * @param stockItemId A stock item id for the request
     */
    static void confirmMerge(final Context context, final int stockItemId) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.label_merge_stock_update)
                .setIcon(R.drawable.icon_merge_dark)
                .setMessage(R.string.label_confirm_merge)
                .setPositiveButton(R.string.label_validate, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MergeStockUpdateRequest.mergeStock(context, stockItemId);
                    }
                })
                .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //nothing to do
                    }
                }).create().show();

    }
}
