package org.papylhomme.assistantaidomenu.ui.stock;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.papylhomme.assistantaidomenu.R;



/**
 * A fragment displaying a list of stock items having a short bbd
 */
public class ListFragmentShortBBDStockItems extends ListFragmentStockItems {



    /**
     * Overridden to hide the unused menu items of ListFragmentStockItems
     *
     * @param menu An instance of menu
     * @param inflater An instance of menu inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem i = menu.findItem(R.id.menu_item_new_stock_item);
        if(i != null) i.setVisible(false);
    }

}
