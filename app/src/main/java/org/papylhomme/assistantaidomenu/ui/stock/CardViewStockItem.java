package org.papylhomme.assistantaidomenu.ui.stock;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBStockItem;

import java.util.Date;



/**
 * A cardview with custom state for stock item's best before date
 */
public class CardViewStockItem extends CardView {

    /**
     * An instance of stock item
     */
    private DBStockItem stockItem;


    /**
     * Default constructor
     *
     * @param context A context
     */
    public CardViewStockItem(Context context) {
        super(context);
    }



    /**
     * Default constructor
     *
     * @param context A context
     * @param attrs Attributes for the cardview
     */
    public CardViewStockItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }



    /**
     * Default constructor
     *
     * @param context A context
     * @param attrs Attributes for the cardview
     * @param defStyleAttr
     */
    public CardViewStockItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    /**
     * Set the stock item for the cardview
     *
     * @param stockItem A stock item instance
     */
    public void setStockItem(DBStockItem stockItem) {
        this.stockItem = stockItem;
        refreshDrawableState();
    }



    /**
     * Register custom state
     *
     * @param extraSpace Extra space for custom states
     * @return An array of state for the cardview
     */
    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        int state[] = super.onCreateDrawableState(extraSpace + 2);

        if(this.stockItem != null && this.stockItem.getBbd() != null) {
            if (stockItem.getBbd().before(new Date()))
                mergeDrawableStates(state, new int[]{R.attr.state_past_bbd});

            else if (stockItem.getBbd().before(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 7)))
                mergeDrawableStates(state, new int[]{R.attr.state_short_bbd});
        }

        return state;
    }
}
