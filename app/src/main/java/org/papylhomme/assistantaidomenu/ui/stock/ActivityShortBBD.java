package org.papylhomme.assistantaidomenu.ui.stock;


import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.db.Query;
import org.papylhomme.assistantaidomenu.provider.requests.ListStockItemRequest;

import java.util.Date;



/**
 * An activity to list stock items with a short or past best before date
 *
 * Add a FAB in lateral pager to trash the stock item
 */
public class ActivityShortBBD extends ActivityStock {

    /**
     * Instantiate the fragment to display the list of stock items
     *
     * @return An updated stock items list fragment
     */
    @Override
    protected ListFragmentStockItems getListFragment() {
        return new ListFragmentShortBBDStockItems();
    }



    /**
     * Create a loader to list the stock items having a short best before date
     *
     * @param i Id of the loader to create
     * @param params Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle params) {
        String bbdColumn = Query.fqcn(DBContract.Stock.TABLE_NAME, DBContract.Stock.COLUMN_BBD);
        String limitDate = Long.toString(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 7).getTime());

        Query.Selection selection = getDefaultSelection()
                .notNull(bbdColumn)
                .condition(bbdColumn, "<", limitDate);

        String sort = Query.formatSort("ASC",
                Query.fqcn(DBContract.Stock.TABLE_NAME, DBContract.Stock.COLUMN_BBD),
                Query.fqcn(DBContract.Stock.TABLE_NAME, DBContract.Stock.COLUMN_NAME)
        );

        return ListStockItemRequest.list(this, selection, sort);
    }
}
