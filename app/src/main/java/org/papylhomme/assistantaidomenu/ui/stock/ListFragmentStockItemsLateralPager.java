package org.papylhomme.assistantaidomenu.ui.stock;

import android.app.Fragment;
import android.os.Bundle;

import org.papylhomme.assistantaidomenu.tools.SiblingsListFragment;

import java.util.ArrayList;



/**
 * A lateral pager for StockItems
 */
public class ListFragmentStockItemsLateralPager extends SiblingsListFragment {


    /**
     * Parameter to filter empty stock
     */
    private static final String PARAM_DISPLAY_EMPTY_STOCK = "EMPTY_STOCK";



    /**
     * Create a new FragmentStockItem for the given product id
     *
     * @param id A product id for the new fragment
     * @return A fragment instance
     */
    @Override
    protected Fragment createSiblingFragment(int id) {
        FragmentStockItem fragmentStock = new FragmentStockItem();
        fragmentStock.setArguments(FragmentStockItem.parameters(id));

        return fragmentStock;
    }



    /**
     * Create a bundle of parameters for a FragmentStockItem instance
     *
     * @return A bundle instance
     */
    public static Bundle parameters(int position, boolean displayEmptyStock, String searchQuery, ArrayList<Integer> groupFilter) {
        Bundle args = new Bundle();
        args.putInt(PARAM_POSITION, position);
        args.putBoolean(PARAM_DISPLAY_EMPTY_STOCK, displayEmptyStock);
        args.putString(PARAM_SEARCH_QUERY, searchQuery);
        args.putIntegerArrayList(PARAM_GROUP_FILTER, groupFilter);
        return args;
    }

}
