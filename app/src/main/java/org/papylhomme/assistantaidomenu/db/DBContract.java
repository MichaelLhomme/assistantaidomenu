package org.papylhomme.assistantaidomenu.db;

import android.annotation.SuppressLint;
import android.provider.BaseColumns;

import java.text.Normalizer;
import java.text.SimpleDateFormat;


/**
 * Database contract
 */
public final class DBContract {

    /**
     * Empty private constructor, prevent instantiation
     */
    private DBContract() {}



    /**
     * Table for product's group
     */
    public static class ProductGroup extends BaseContract {

        public static final String TABLE_NAME = "product_group";

        public static final String COLUMN_NAME = "group_name";


        /**
         * Create table statement
         */
        static final String SQL_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME + " TEXT)";

        /**
         * Drop table statement
         */
        static final String SQL_DROP =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }


    /**
     * Table for products
     */
    public static class Products extends BaseContract {

        public static final String TABLE_NAME = "products";

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_NORMALIZED_NAME = "normalized_name";
        public static final String COLUMN_GROUP_ID = "group_id";
        public static final String COLUMN_UNIT = "unit";
        public static final String COLUMN_BRAND = "brand";


        /**
         * Create table statement
         */
        static final String SQL_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME + " TEXT," +
                        COLUMN_NORMALIZED_NAME + " TEXT," +
                        COLUMN_GROUP_ID + " INTEGER," +
                        COLUMN_UNIT + " TEXT," +
                        COLUMN_BRAND + " TEXT)";

        /**
         * Drop table statement
         */
        static final String SQL_DROP =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }



    /**
     * Table for stock items
     */
    public static class Stock extends BaseContract {

        @SuppressWarnings("WeakerAccess")
        @SuppressLint("SimpleDateFormat")
        public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

        public static final String TABLE_NAME = "stock";

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_NORMALIZED_NAME = "normalized_name";
        public static final String COLUMN_GROUP_ID = "group_id";
        public static final String COLUMN_UNIT = "unit";
        public static final String COLUMN_BRAND = "brand";
        public static final String COLUMN_BBD = "best_before";
        public static final String COLUMN_INITIAL = "initial";
        public static final String COLUMN_CURRENT = "current";
        public static final String COLUMN_TIMESTAMP = "timestamp";


        /**
         * Create table statement
         */
        static final String SQL_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME + " TEXT," +
                        COLUMN_NORMALIZED_NAME + " TEXT," +
                        COLUMN_GROUP_ID + " INTEGER," +
                        COLUMN_UNIT + " TEXT," +
                        COLUMN_BRAND + " TEXT," +
                        COLUMN_BBD + " INTEGER," +
                        COLUMN_INITIAL + " REAL," +
                        COLUMN_CURRENT + " REAL," +
                        COLUMN_TIMESTAMP + " INTEGER)";


        /**
         * Drop table statement
         */
        static final String SQL_DROP =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }



    /**
     * Table for stock update
     */
    public static class StockUpdate extends BaseContract {

        public static final String TABLE_NAME = "stock_update";

        public static final String COLUMN_STOCK_ITEM_ID = "stock_item";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_TIMESTAMP = "timestamp";

        /**
         * Create table statement
         */
        static final String SQL_CREATE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_STOCK_ITEM_ID + " INTEGER," +
                        COLUMN_TYPE + " TEXT," +
                        COLUMN_AMOUNT + " REAL," +
                        COLUMN_TIMESTAMP + " INTEGER)";

        /**
         * Drop table statement
         */
        static final String SQL_DROP =
                "DROP TABLE IF EXISTS " + TABLE_NAME;

    }



    /**
     * Base contract for every table
     */
    private static class BaseContract implements BaseColumns {

        /**
         * Normalize a string
         *
         * @param source The source string
         * @return The normalized string
         */
        public static String normalizeString(String source) {
            return Normalizer.normalize(source, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toLowerCase();
        }

    }
}
