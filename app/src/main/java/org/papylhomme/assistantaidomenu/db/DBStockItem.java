package org.papylhomme.assistantaidomenu.db;

import android.database.Cursor;

import org.papylhomme.aidomenu.StockItem;

import java.util.Date;



/**
 * Extends the base stock item to include the database's id
 */
public class DBStockItem extends StockItem {

    /**
     * The stock item id
     */
    private int id;


    /**
     * A timestamp for the initial value of the stock item
     */
    private long initialTimestamp;


    /**
     * The product's brand
     */
    private final String brand;


    /**
     * The best before date
     */
    private final Date bbd;


    /**
     * The formatted best before date
     */
    private final String formattedBbd;



    /**
     * Constructor
     *
     * @param c A cursor instance
     */
    public DBStockItem(Cursor c) {
        super(
                c.getString(c.getColumnIndex(DBContract.Stock.COLUMN_NAME)),
                c.getString(c.getColumnIndex(DBContract.ProductGroup.COLUMN_NAME)),
                c.getString(c.getColumnIndex(DBContract.Stock.COLUMN_UNIT)),
                c.getDouble(c.getColumnIndex(DBContract.Stock.COLUMN_INITIAL)),
                c.getDouble(c.getColumnIndex(DBContract.Stock.COLUMN_CURRENT))
        );

        id = c.getInt(c.getColumnIndex(DBContract.Stock._ID));
        initialTimestamp = c.getLong(c.getColumnIndex(DBContract.Stock.COLUMN_TIMESTAMP));
        brand = c.getString(c.getColumnIndex(DBContract.Stock.COLUMN_BRAND));

        //format the best before date
        String tmpBBD = c.getString(c.getColumnIndex(DBContract.Stock.COLUMN_BBD));

        if(tmpBBD == null || tmpBBD.isEmpty()) {
            bbd = null;
            formattedBbd = "";
        } else {
            bbd = new Date(Long.parseLong(tmpBBD));
            formattedBbd = DBContract.Stock.dateFormatter.format(bbd);
        }
    }



    /**
     * Id getter
     *
     * @return The stock item id
     */
    public int getId() {
        return id;
    }



    /**
     * Initial timestamp getter
     *
     * @return The initial timestamp
     */
    public long getInitialTimestamp() {
        return initialTimestamp;
    }



    /**
     * Best before date accessor
     */
    public Date getBbd() {
        return bbd;
    }



    /**
     * Formatted best before date accessor
     *
     * @return The best before date
     */
    public String getFormattedBbd() {
        return formattedBbd;
    }



    /**
     * Brand accessor
     *
     * @return The brand
     */
    public String getBrand() {
        return brand;
    }
}
