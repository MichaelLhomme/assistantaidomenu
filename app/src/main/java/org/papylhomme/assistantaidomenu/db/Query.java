package org.papylhomme.assistantaidomenu.db;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import java.util.LinkedList;
import java.util.List;



/**
 * Helper class to generate SQL SELECT queries
 *
 */
public class Query {

    /**
     * A StringBuilder for the query
     */
    private StringBuilder builder = new StringBuilder();


    /**
     * Arguments for the query
     */
    private String[] args = null;



    /**
     * Create a new SELECT query on table using the given projection
     *
     * @param table The targeted table
     * @param projection A projection's list
     */
    public Query(String table, String[] projection) {
        builder.append("SELECT ");
        formatProjection(projection);
        builder.append(" FROM ");
        builder.append(table);
    }



    /**
     * Generate a fully qualified column name
     *
     * @param table The targeted table
     * @param column The targeted column
     * @return "$table.$column"
     */
    public static String fqcn(String table, String column) {
        return table + "." + column;
    }



    /**
     * Fully qualified column name to column name for projection
     *
     * Map the fqcn to column's name
     *
     * @param table The targeted column
     * @param column The targeted column
     * @return "$table.column AS $column"
     */
    public static String fqcn2n(String table, String column) {
        return fqcn(table, column) + " AS " + column;
    }



    /**
     * Create an empty cursor
     *
     * @return An empty cursor
     */
    public static Cursor emptyCursor() {
        return new MatrixCursor(new String[0]);
    }



    /**
     * Format the SORT clause
     *
     * @param order A sort order
     * @param columns A list of columns for the sort
     * @return A sort clause
     */
    public static String formatSort(String order, String... columns) {
        if(columns == null || columns.length == 0)
            return null;

        StringBuilder builder = new StringBuilder();

        for(String column : columns) {
            if(builder.length() > 0)
                builder.append(", ");

            builder.append(column);
        }

        builder.append(" ").append(order);

        return builder.toString();
    }



    /**
     *  Add a JOIN clause to the query
     *
     * @param local Local table for ON condition
     * @param localId Local column for ON condition
     * @param remote Remote table for ON condition
     * @param remoteId Remote column for ON condition
     * @return The query instance for chaining
     */
    public Query join(String local, String localId, String remote, String remoteId) {
        formatJoin(local, localId, remote, remoteId);
        return this;
    }



    /**
     *  Add a JOIN clause to the query
     *
     * @param type The JOIN type
     * @param local Local table for ON condition
     * @param localId Local column for ON condition
     * @param remote Remote table for ON condition
     * @param remoteId Remote column for ON condition
     * @return The query instance for chaining
     */
    public Query join(String type, String local, String localId, String remote, String remoteId) {
        formatJoin(type, local, localId, remote, remoteId);
        return this;
    }



    /**
     * Add a WHERE clause to the query
     *
     * @param selection A selection string
     * @param args Arguments for selection
     * @return The query instance for chaining
     */
    public Query selection(String selection, String[] args) {
        formatSelection(selection);
        this.args = args;
        return this;
    }



    /**
     * Add a SORT clause to the query
     *
     * @param sortOrder A sort order as returned by formatSort
     * @return The query instance for chaining
     */
    public Query sort(String sortOrder) {
        if(sortOrder != null && !sortOrder.isEmpty())
            builder.append(" ORDER BY ").append(sortOrder);
        return this;
    }



    /**
     * Add a GROUP BY clause to the query
     *
     * @param column The targeted column
     * @return The query instance for chaining
     */
    public Query group(String column) {
        builder.append(" GROUP BY ").append(column);
        return this;
    }



    /**
     * Add an HAVING clause to the query
     *
     * @param condition A condition for the HAVING
     * @return The query instance for chaining
     */
    public Query having(String condition) {
        builder.append(" HAVING ").append(condition);
        return this;
    }




    /**
     * Format a projection into a query string builder
     *
     * @param projection An array for projection
     */
    private void formatProjection(String[] projection) {
        if(projection == null)
            builder.append("*");

        else {
            StringBuilder b = new StringBuilder();
            for (String column : projection) {
                if (b.length() > 0) b.append(", ");
                b.append(column);
            }
            builder.append(b);
        }
    }



    /**
     * Format a JOIN clause into a request builder
     *
     * @param local Local table for ON condition
     * @param localId Local column for ON condition
     * @param remote Remote table for ON condition
     * @param remoteId Remote column for ON condition
     */
    private void formatJoin(String local, String localId, String remote, String remoteId) {
        formatJoin("JOIN", local, localId, remote, remoteId);
    }



    /**
     * Format a JOIN clause into the request builder
     *
     * @param joinType The JOIN type
     * @param local Local table for ON condition
     * @param localId Local column for ON condition
     * @param remote Remote table for ON condition
     * @param remoteId Remote column for ON condition
     */
    private void formatJoin(String joinType, String local, String localId, String remote, String remoteId) {
        builder.
                append(" ").append(joinType).append(" ").append(remote).
                append(" ON ").append(fqcn(local, localId)).append(" = ").append(fqcn(remote, remoteId));
    }



    /**
     * Format a selection string into the request builder
     *
     * @param selection A selection string
     */
    private void formatSelection(String selection) {
        if(selection != null && !selection.isEmpty())
            builder.append(" WHERE ").append(selection);
    }





    /**
     * Execute the query on the given database
     *
     * @param db A database instance
     * @return A cursor for results
     */
    public Cursor execute(SQLiteDatabase db) {
        String sql = builder.toString();

        //check the statement is valid to prevent app crash caused by the db
        try {
            db.compileStatement(sql);
            return db.rawQuery(sql, args);
        } catch(Exception ex) {
            Log.e(Query.class.getName(), "Error execute query", ex);
            return emptyCursor();
        }
    }



    /**
     * Helper class to generate SQL WHERE clauses
     */
    public static class Selection {

        /**
         * A StringBuilder for the selection string
         */
        private StringBuilder builder = new StringBuilder();


        /**
         * A list for arguments
         */
        private LinkedList<String> arguments = new LinkedList<>();



        /**
         * Add a not null condition
         *
         * @param column The targeted column
         * @return The selection instance for chaining
         */
        public Selection notNull(String column) {
            if(builder.length() > 0) builder.append(" AND ");
            builder.append(column).append(" ").append(" NOT NULL");
            return this;
        }



        /**
         * Add a condition to the selection
         *
         * @param column The targeted column
         * @param condition The type of condition
         * @param value The value for the condition
         * @return The selection instance for chaining
         */
        public Selection condition(String column, String condition, String value) {
            if(builder.length() > 0) builder.append(" AND ");
            builder.append(column).append(" ").append(condition).append(" ?");
            arguments.add(value);
            return this;
        }



        /**
         * Add an '=' clause to the selection
         *
         * @param column The targeted column
         * @param value The value to match
         * @return The selection instance for chaining
         */
        public Selection match(String column, String value) {
            return value == null ? this : this.condition(column, "=", value);
        }



        /**
         * Add a GLOB clause to the selection
         *
         * @param column The targeted column
         * @param pattern The pattern to match
         * @return The selection instance for chaining
         */
        public Selection glob(String column, String pattern) {
            return pattern == null ? this : this.condition(column, "GLOB", "*" + pattern + "*");
        }



        /**
         * Add an IN clause to the selection
         *
         * @param column The targeted column
         * @param values A list of values
         * @return The selection instance for chaining
         */
        public Selection in(String column, List values) {
            if(values == null)
                return this;

            StringBuilder inSelection = new StringBuilder("(");
            for(Object value : values) {
                if(inSelection.length() > 1) inSelection.append(",");
                inSelection.append(value == null ? null : value.toString());
            }
            inSelection.append(")");

            if (builder.length() > 0) builder.append(" AND ");
            builder.append(column).append(" IN ").append(inSelection.toString());

            return this;
        }



        /**
         * Retrieve the selection string
         *
         * @return The selection string
         */
        public String getString() {
            return builder.toString();
        }



        /**
         * Retrieve selection arguments
         *
         * @return A string array of arguments
         */
        public String[] getArguments() {
            return arguments.toArray(new String[arguments.size()]);
        }

    }
}
