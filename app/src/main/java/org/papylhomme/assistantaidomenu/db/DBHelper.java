package org.papylhomme.assistantaidomenu.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static org.papylhomme.assistantaidomenu.db.DBContract.ProductGroup;
import static org.papylhomme.assistantaidomenu.db.DBContract.Products;
import static org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import static org.papylhomme.assistantaidomenu.db.DBContract.StockUpdate;


/**
 * Helper for the database
 */
public class DBHelper extends SQLiteOpenHelper {

    /**
     * Schema version
     */
    private static final int DATABASE_VERSION = 12;


    /**
     * Database name
     */
    private static final String DATABASE_NAME = "AssistantAidomenu.db";



    /**
     * Constructor
     *
     * @param context A context instance
     */
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    /**
     * Initialize the database structure
     *
     * @param db A database instance
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ProductGroup.SQL_CREATE);
        db.execSQL(Products.SQL_CREATE);
        db.execSQL(Stock.SQL_CREATE);
        db.execSQL(StockUpdate.SQL_CREATE);
    }



    /**
     * Upgrade the database
     *
     * @param db A database instance
     * @param oldVersion Old schema version
     * @param newVersion New schema version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(true) {
            //default is to discard and recreate from scratch
            db.execSQL(ProductGroup.SQL_DROP);
            db.execSQL(Products.SQL_DROP);
            db.execSQL(Stock.SQL_DROP);
            db.execSQL(StockUpdate.SQL_DROP);

            onCreate(db);
        }
    }
}
