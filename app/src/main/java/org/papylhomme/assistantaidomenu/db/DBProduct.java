package org.papylhomme.assistantaidomenu.db;

import android.database.Cursor;

import org.papylhomme.aidomenu.Product;

/**
 * Extends the base product to include the database's id
 */
public class DBProduct extends Product {

    /**
     * The product id
     */
    private int id;


    /**
     * The unit for the product
     */
    private  String unit;



    /**
     * Constructor
     *
     * @param c A cursor instance
     */
    public DBProduct(Cursor c) {
        super(
                c.getString(c.getColumnIndex(DBContract.Products.COLUMN_NAME)),
                c.getString(c.getColumnIndex(DBContract.ProductGroup.COLUMN_NAME)),
                c.getString(c.getColumnIndex(DBContract.Products.COLUMN_BRAND))
        );

        id = c.getInt(c.getColumnIndex(DBContract.Products._ID));
        unit = c.getString(c.getColumnIndex(DBContract.Products.COLUMN_UNIT));
    }



    /**
     * Id getter
     *
     * @return The product id
     */
    public int getId() {
        return id;
    }



    /**
     * Unit getter
     *
     * @return The product unit
     */
    public String getUnit() {
        return unit;
    }
}
