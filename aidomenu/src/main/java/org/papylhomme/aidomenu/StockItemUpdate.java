package org.papylhomme.aidomenu;

/**
 *
 */
public class StockItemUpdate {

    /**
     *
     */
    private String type;


    /**
     *
     */
    private double quantity;



    /**
     *
     * @param type
     * @param quantity
     */
    public StockItemUpdate(String type, double quantity) {
        this.type = type;
        this.quantity = quantity;
    }



    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }



    /**
     *
     * @return
     */
    public double getQuantity() {
        return quantity;
    }



    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.type + " : " + this.quantity;
    }
}
