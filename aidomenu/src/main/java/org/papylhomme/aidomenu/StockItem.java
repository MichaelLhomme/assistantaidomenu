package org.papylhomme.aidomenu;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class StockItem {

    /**
     *
     */
    protected String name;


    /**
     *
     */
    protected String group;


    /**
     *
     */
    protected double initialQuantity;


    /**
     *
     */
    protected double currentQuantity;


    /**
     *
     */
    protected String unit;


    /**
     *
     */
    private List<StockItemUpdate> mouvements = new ArrayList<>();



    /**
     *
     */
    protected StockItem() {
    }



    /**
     *
     * @param name
     * @param initialQuantity
     * @param unit
     */
    public StockItem(String name, String group, String unit, double initialQuantity) {
        this(name, group, unit, initialQuantity, initialQuantity);
    }



    /**
     *
     * @param name
     * @param unit
     * @param initialQuantity
     */
    public StockItem(String name, String group, String unit, double initialQuantity, double currentQuantity) {
        this.name = name;
        this.group = group;
        this.unit = unit;

        this.initialQuantity = initialQuantity;
        this.currentQuantity = currentQuantity;
    }



    /**
     *
     * @param type
     * @param quantity
     */
    public void update(String type, double quantity) {
        this.mouvements.add(new StockItemUpdate(type, quantity));
        this.currentQuantity += quantity;
    }



    /**
     *
     * @param quantity
     */
    public void set(double quantity) {
        //skip if current quantity is already correct
        if(this.currentQuantity == quantity)
            return;

        update("Ecart de stock", quantity - this.currentQuantity);
    }



    /**
     *
     * @return
     */
    public List<StockItemUpdate> getMouvements() {
        return mouvements;
    }



    /**
     * Name getter
     *
     * @return The name
     */
    public String getName() {
        return name;
    }



    /**
     * Unit getter
     *
     * @return The unit
     */
    public String getUnit() {
        return unit;
    }



    /**
     * Group getter
     *
     * @return The group
     */
    public String getGroup() {
        return group;
    }



    /**
     * Current quantity getter
     *
     * @return The current quantity
     */
    public double getCurrentQuantity() {
        return currentQuantity;
    }



    /**
     * Initial quantity getter
     *
     * @return The initial quantity
     */
    public double getInitialQuantity() {
        return initialQuantity;
    }



    /**
     *
     * @return
     */
    public String toString() {
        return this.name + " (" + this.currentQuantity + " " + this.unit + ")";
    }
}
